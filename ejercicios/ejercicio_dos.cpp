#include <conio.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
using namespace std;

class ALUMNO{
  int codigo;
  char nombre[40];
  public:
     void IngresarAlumno(){
         cout<<"\n leer codigo : ";cin>>codigo;
         cout<<"\n leer nombre : ";cin>>nombre;
     }       
     void MostrarAlumno(){
         cout<<"\n "<<codigo<<"      "<<nombre; 
     }
     void REGISTRAR(char fila[],ALUMNO x){
       ofstream esc(fila,ios::app);
       if(!esc){  cout<<"\n ERROR al abrir "<<fila;getch();    }
       else{
          esc.write(reinterpret_cast<char *>(&x),sizeof(ALUMNO));
          esc.close();
       }
     }  
     void  VISUALIZAR(char fila[]){
        ALUMNO y;
        ifstream lec(fila);
        if(!lec){        cout<<"\n ERROR al abrir "<<fila;getch();     }
        else {
          cout<<"\n MOSTRANDO DATOS DEL ARCHIVO "<<fila;   
          lec.read(reinterpret_cast<char *>(&y),sizeof(ALUMNO));
          while(!lec.eof()){
                      y.MostrarAlumno();
	        lec.read(reinterpret_cast<char *>(&y),sizeof(ALUMNO));
          }
	      lec.close();
        }
     } 
     bool INSERTARALUMNO(char fila1[],char fila2[],int posicion, ALUMNO alumno){
       ALUMNO y;
       int indice = 0;
       bool insertado = false;
       ifstream F1(fila1);
       ofstream F2(fila2, ios::out|ios::trunc |ios::binary);
       if(!F1){  cout<<"\n ERROR al abrir "<<fila1;getch();    }
       else{
          if(!F1){        cout<<"\n ERROR al abrir "<<fila1;getch();     }
          else {
             F1.read(reinterpret_cast<char *>(&y),sizeof(ALUMNO));
             while(!F1.eof()){
                         if(indice == posicion){
                            F2.write(reinterpret_cast<char *>(&alumno),sizeof(ALUMNO));
                            insertado = true;
                         }
                         F2.write(reinterpret_cast<char *>(&y),sizeof(ALUMNO));
	            F1.read(reinterpret_cast<char *>(&y),sizeof(ALUMNO));
                indice = indice + 1;
             }
	         F1.close();
          }  
          F1.close();
          F2.close();
       }
       return insertado;
    }
};

int main(int argc, char *argv[]){
    char op;
    ALUMNO a;
    for(;;){
       cout<<" \n adicionar  fila1  <1>";
       cout<<" \n Mostrar    filas  <2>";
       cout<<" \n Insertar segun posicion  <3>";
       cout<<" \n salir             <4>";
       op=getch();
       switch(op){
       case '1':a.IngresarAlumno();
                a.REGISTRAR("fila1",a);break;
       case '2':a.VISUALIZAR("fila1");
                a.VISUALIZAR("fila2");
                getch(); break;
       case '3':
                int p;
                cout<<" \n Ingrese posicion";
                cin>>p;
                ALUMNO tmpa;
                tmpa.IngresarAlumno();
                a.INSERTARALUMNO("fila1","fila2",p,tmpa);
                break;
       case '4':return 0;
       }
     }
}
/*
  Alumnos:
  Cumpa Ascu�a, Miguel Angel
  Chavez Albornoz, Piter Enrique
  Villanueva Huapaya, Carlos
  Tuanama D�vila, Helen
*/
