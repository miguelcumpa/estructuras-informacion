#include <conio.h>
#include <iostream>
#include <fstream>
#include <string.h>

/*

Especificación ARREGLOENTERO
variable
arreglo: arruno
arreglo: arrdos
entero:td
entero:tu
entero:tm
operaciones
setEnterosUno                      : no retorna valor
setEnterosDos                       : no retorna valor
setUnoEnDos                   : no retorna valor
significado
En setEnterosUno se carga de datos el primer array
En setEnterosDos se carga de datos el primer array
En setUnoEnDos  se carga el array uno en el array dos de acuerdo a una posición
Fin_ARREGLOENTERO

funcion setUnoEnDos:
      entero posicion
      escribir "ingrese la posicion"
      leer posicion
      arreglo tmparr[100]
      
      para z <- 0 hasta td  hacer
			tmparr[z] = arrdos[z]
	  fin para
      entero tm <- 0
      
      para j <- 0 hasta td  hacer
			si (j+1) mod 2 > 0 entonces
			  para x <- 0 hasta tu hacer
			    arrdos[tm] = arruno[x]
			    tm <- tm + 1
			  fin para
			 fin si
			arrdos[tm] <- tmparr[j]
			tm <- tm+1
	  fin para
      
       escribir "Mostrando nuevos valores del array"
       para y <- 0 hasta tm hacer
         escribir arrdos[y]
       finpara
 finsetUnoEnDos

*/
using namespace std;

class ARREGLOENTERO{
	int arruno[100];
	int arrdos[100];
	int tu,td,tm;
	public:
		void setEnterosUno(){
			int tamano;
			cout<<"\n Ingrese cantidad de numero a ingresar : ";cin>>tamano;
			tu = tamano;
			for(int i=0;i<tamano;i++){
				cout<<"\n Ingrese numero : ";cin>>arruno[i];
			}
		}
		void setEnterosDos(){
			int tamano;
			cout<<"\n Ingrese cantidad de numero a ingresar : ";cin>>tamano;
			td = tamano;
			for(int i=0;i<tamano;i++){
				cout<<"\n Ingrese numero : ";cin>>arrdos[i];
			}
		}
		void setUnoEnDos(){
			int tmparr[100];
			for(int z=0;z<td;z++){
                tmparr[z] = arrdos[z];
            }
			tm = 0;
			for(int j=0;j<td;j++){
				if(((j+1)%2) > 0){
					for(int x=0;x<tu;x++){
						arrdos[tm] = arruno[x];
						tm = tm+1;
					}
				}
				arrdos[tm] = tmparr[j];
				tm = tm+1;
			}
			cout<<"\n Mostrando nuevos valores del array \n";
			for(int y=0;y<tm;y++){
				cout<<" "<<arrdos[y];
			}
		}
};

main(){
    ARREGLOENTERO a;
     for(;;){
     cout<<"\n FISC"<<"\n <1> Ingresar arreglo 1 \n <2> Ingresar arreglo 2 \n <3> Combinar arrays <4> Salir \n Ingrese su Opcion: ";
     char op;
     cin>>op;
     switch(op){
     case '1':cout<<"\n";
              cout<<"\n ************ARREGLO UNO*************"<<endl;
              a.setEnterosUno();
              cout<<"\n **************************************************************"<<endl;
              break;
     case '2':cout<<"\n";
              cout<<"\n ******************************ARREGLO DOS**************************"<<endl;
              a.setEnterosDos();
              cout<<"\n **************************************************************"<<endl;
              break;
	case '3':cout<<"\n";
              cout<<"\n ******************************COMBINAR ARREGLOS**************************"<<endl;
              a.setUnoEnDos();
              cout<<"\n **************************************************************"<<endl;
              break;
     case '4':cout<<"\n";
              cout<<"\n ************************Bye**************************"<<endl;
              return 0;
     }
    }
}
