#include <conio.h>
#include <iostream>
#include <fstream>
#include <string.h>

/*

Especificaci�n AUTO
variable
cadena   : numero_placa
cadena : color
operaciones
IngresarAuto                      : no retorna valor
Del_Auto                       : no retornar valor
MostrarAuto                      : no retornar valor
getNumero_Placa : retorna valor cadena
significado
getNumero_Placa, retorna el numero de placa
Fin_AUTO


Especificaci�n ALUMNO
Usa AUTO
variable
entero   : codigo
cadena : nombre
cadena : apellido
AUTO : lista
operaciones
IngresarAlumno                      : no retorna valor
MostrarDatos                       : no retornar valor
EliminarMovil(placa) : no retornar valor
BuscarMovil(placa) : no retornar valor
significado
En EliminarMovil, elimina el auto cuya placa sea igual a la placa ingresada y muestra un resumen del due�o
En BuscarMovil, busca el auto cuya placa sea igual a la placa ingresada y muestra un resumen del due�o
Fin_ALUMNO

*/
using namespace std;

class AUTO{
      char numero_placa[50];
      char color[50];
      public:
          void IngresarAuto(){
             cout<<"\n Ingrese n�mero de placa : ";cin>>numero_placa;
             cout<<"\n Ingrese color : ";cin>>color;
          }
          char *getNumero_Placa(){
               return numero_placa;
          }
          void Del_Auto(){
               strcpy(numero_placa,"eliminado");
               strcpy(color,"eliminado");
          }
          void MostrarAuto(){
             cout<<"\n n�ero de placa : "<<numero_placa;
             cout<<"\n color : "<<color;
          }
};

class ALUMNO{
      int codigo;
      char nombre[300];
      char apellido[300];
      AUTO *autos;
      public:
          void IngresarAlumno(){
             int n;
             cout<<"\n Ingrese codigo : ";cin>>codigo;
             cout<<"\n Ingrese nombre : ";cin>>nombre;
             cout<<"\n Ingrese apellidos: ";cin>>apellido;
             cout<<"\n Ingrese N� de autos del alumno : ";cin>>n;
             autos = new AUTO[n];
             for(int i=0;i<n;i++){
                 AUTO a;
                 a.IngresarAuto();
                 autos[i] = a;
             }
          }
          void MostrarDatos(){
             cout<<"\n codigo : "<<codigo;
             cout<<"\n nombre : "<<nombre;
             cout<<"\n apellidos: "<<apellido;
          }
          void EliminarMovil(char placa[50]){
               for(int j=0;j<sizeof(autos)-1;j++){
                   if(strncmp(autos[j].getNumero_Placa(),placa,50)==0){
                       cout<<"\n Eliminar : "<<autos[j].getNumero_Placa()<<endl;
                       autos[j].Del_Auto();
                       cout<<"\n **********************Alumno********************";
                       MostrarDatos();
                       for(int x=0;x<sizeof(autos)-1;x++){
                             if(strncmp(autos[x].getNumero_Placa(),"eliminado",50)==-1){
                                 cout<<"\n **********************Auto********************";
                                 autos[x].MostrarAuto();
                             }
                       }
                   }
               }
          }
          void BuscarMovil(char placa[50]){
               for(int x=0;x<sizeof(autos)-1;x++){
                     if(strncmp(autos[x].getNumero_Placa(),placa,50)==0){
                           cout<<"\n **********************Alumno********************";
                           MostrarDatos();
                           for(int x=0;x<sizeof(autos)-1;x++){
                                 if(strncmp(autos[x].getNumero_Placa(),"eliminado",50)==-1){
                                     cout<<"\n **********************Auto********************";
                                     autos[x].MostrarAuto();
                                 }
                           }
                     }
               }
          }
 };
   
main(){
     int n;
     cout<<"\n Ingrese N� de alumnos : ";cin>>n;
     ALUMNO a[n];
     for(int i=0;i<n;i++){
         ALUMNO alu;
         alu.IngresarAlumno();
         a[i] = alu;
     }
     char placa[50];
     for(;;){
     cout<<"\n FISC"<<"\n <1> Buscar auto \n <2> Eliminar auto \n <3> Salir  \n Ingrese su Opcion: ";
     char op;
     cin>>op;
     switch(op){
     case '1':cout<<"\n";
              cout<<"\n ************BUSCAR AUTO*************"<<endl;
              cout<<"\n Ingrese numero de placa: "; cin>>placa;
              for(int i=0;i<n;i++){
                  a[i].BuscarMovil(placa);
              }
              cout<<"\n **************************************************************"<<endl;
              break;
     case '2':cout<<"\n";
              cout<<"\n ******************************Eliminar**************************"<<endl;
              cout<<"\n Ingrese numero de placa: "; cin>>placa;
              for(int i=0;i<n;i++){
                  a[i].EliminarMovil(placa);
              }
              cout<<"\n **************************************************************"<<endl;
              break;

     case '3':cout<<"\n";
              cout<<"\n ************************Bye**************************"<<endl;
              return 0;
     } 
    }
}
