#include <conio.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include <time.h>

using namespace std;

/*Cajero*/
class CAJERO{
    public:
    char dni[8];
        char apellido[255];
        char nombre[255];
        char cajero[25];
        char pass[20];
        bool logged;
        CAJERO (){logged = false;}
     bool login(char iscajero[25], char ispass[20]){
     if(strncmp(cajero,iscajero,25)==0 && strncmp(pass,ispass,20)==0){
              logged = true;
              cout<<"\n Bienvenido Sr.: "<<nombre<<", "<<apellido;
              return true;
          }
          return false;
     }
     void cajeroregistrar(){
            cout<<"\n DNI: ";cin>>dni;
            cout<<"\n Apellidos: ";cin>>apellido;
            cout<<"\n Nombres: ";cin>>nombre;
            cout<<"\n usuario: ";cin>>cajero;
            cout<<"\n password: ";cin>>pass;
        }
};

class FECHA{
  char fecha[80];
  public:
    FECHA(){}
      char *getFecha(){
        time_t rawtime;
        struct tm * timeinfo;
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        strftime (fecha,80,"%c",timeinfo);
        return fecha;
      }
      char *getFechaSaved(){return fecha;}
};

/*Cliente*/
class CLIENTE{
    char apellido[300];
    char nombre[300];
    char direccion[600];
    char dni[8];
    
    public:

	void setDNI(char sdni[8]){
		strcpy(dni,sdni);
		}
	char *getDNI(){
		return dni;
		}
    void MostrarClienteDatos(){
         cout<<"DNI :"<<dni<<"\n";
         cout<<"Cliente :"<<apellido<<nombre<<"\n";
         cout<<"Direcci�n :"<<direccion<<"\n";
    }
    void MostrarCliente(){
         cout<<"DNI :"<<dni<<"\n";
         cout<<"Cliente :"<<apellido<<nombre<<"\n";
    }
    void IngresarCliente(){
        cout<<"\n DNI: ";cin>>dni;
        cout<<"\n Apellidos: ";cin>>apellido;
        cout<<"\n Nombres: ";cin>>nombre;
        cout<<"\n Domiclio: ";cin>>direccion;
    }
};

/*Movimientos*/
class MOVIMIENTOS{
        
        char mdni[8];
        char movimiento[15];
        double monto;
        double total;
        char cajero[30];
        char fecha[20];
    public:
    
        char *getMDNI(){return this->mdni;}
        char *getMovimiento(){return this->movimiento;}
        double getMonto(){return this->monto;}
        double getTotal(){return this->total;}
        char *getCajero(){return this->cajero;}
        char *getFecha(){return this->fecha;}
        
        void setMDNI(char d[8]){ strcpy(mdni,d);}
        void setMovimiento(char mo[15]){ strcpy(movimiento,mo);}
        void setMonto(double m){monto = m;}
        void setTotal(double t){total = t;}
        void setCajero(char ca[30]){ strcpy(cajero,ca);}
        void setFecha(char f[20]){ strcpy(fecha,f);}
        void IngLista( char d[8], char mo[15], double m,double t, char ca[30], char f[20]){
             MOVIMIENTOS mov;
            mov.setMDNI(d);
            mov.setMovimiento(mo);
            mov.setMonto(m);
            mov.setTotal(t);
            mov.setCajero(ca);
            mov.setFecha(f);
            ofstream F3("movimientos.txt", ios::out|ios::binary);
              if(!F3){  cout<<"\n ERROR al abrir ";    }
              else{
                   F3.write((char*)&mov, sizeof(mov));
              }
	          F3.close();
        }
        void VisLista(char mbdni[8]){
             MOVIMIENTOS ca;
            ifstream lec("movimientos.txt",ios::binary);
            if(!lec){  cout<<"\n ERROR al abrir "; }
            else {
              lec.read((char*)&ca, sizeof(ca));
              cout<<"*********REPORTE DE MOVIMIENTOS******************\n";
              while(!lec.eof()){
                   if(strncmp(ca.getMDNI(),mbdni,8)==0){
                          cout<<"| DNI | CLIENTE | MONTO | TOTAL | CAJERO | FECHA |";
                          cout<<"| "<<ca.getMDNI()<<" | "<<ca.getMovimiento()<<" | "<<ca.getMonto()<<" | "<<ca.getTotal()<<" | "<<ca.getCajero()<<" | "<<ca.getFecha()<<" |";
                   }
                   lec.read((char*)&ca, sizeof(ca));
              }
              lec.close();
            }
        }
};

/*Cuenta de Ahorro*/
class CUENTAAHORRO{
	char tipoCta;
	FECHA fecha_registro;
	CLIENTE *cliente;
	CAJERO cajero;
	int numerocuenta,nclientes;
	double montoahorro;

    public:
        CUENTAAHORRO(){}
        
        bool buscarCliente(char dni[8]){
             for(int i=0; i<nclientes;i++){
                 if(strncmp(cliente[i].getDNI(),dni,8)==0){
                     return true;
                 }
             }
             return false;
        }

        void retirarDinero(char dnicliente[8]){
             MOVIMIENTOS m;
             double montoretiro;
             CUENTAAHORRO ca;
             FECHA f;
             ifstream lec("cuentas.txt",ios::binary);
             ofstream F2("cuentas_tmp.txt", ios::out| ios::trunc|ios::binary);
             if(!lec){  cout<<"\n ERROR al abrir "; }
             else {
                  lec.read((char*)&ca, sizeof(ca));
                  while(!lec.eof()){
                       if(ca.buscarCliente(dnicliente)){
                             do{
                                cout<<"\n Ingrese monto a retirar";
                                cin>>montoretiro;
                             }while(montoretiro > ca.montoahorro);
                             cout<<"Monto Actual : "<<ca.montoahorro<<"\n";
                             cout<<"Monto a retirar : "<<montoretiro<<"\n";
                             cout<<"Descuento por retiro : "<<ca.montoahorro*0.005<<"\n";
                             ca.montoahorro = ca.montoahorro - (ca.montoahorro*0.005) - montoretiro;
                             cout<<"Monto registrado : "<<ca.montoahorro<<"\n";
                             cout<<"\n Retiro efectuado correctamente.";
                             m.IngLista(dnicliente,"Retiro",montoretiro,ca.montoahorro,ca.cajero.cajero,f.getFecha());
                             ca.verCuentaAhorro();
                       }
                       F2.write((char*)&ca, sizeof(ca));
	                   lec.read((char*)&ca, sizeof(ca)); 
                  }
	              lec.close();
	              F2.close();
	              inFile();
             }
        }
        
        void abonarDinero(char dnicliente[8]){
           CUENTAAHORRO  ca;
           MOVIMIENTOS m;
           double montoabono;
           FECHA f;
           ifstream lec("cuentas.txt",ios::binary);
           ofstream F2("cuentas_tmp.txt", ios::out| ios::trunc|ios::binary);
             if(!lec){  cout<<"\n ERROR al abrir "; }
             else {
                  lec.read((char*)&ca, sizeof(ca));
                  while(!lec.eof()){
                      if(ca.buscarCliente(dnicliente)){
                             do{
                                cout<<"\n Ingrese monto a abonar";
                                cin>>montoabono;
                             }while(montoabono < 1);
                             cout<<"Monto Actual : "<<ca.montoahorro<<"\n";
                             cout<<"Monto a abonar : "<<montoabono<<"\n";
                             cout<<"Descuento por abono : "<<ca.montoahorro*0.005<<"\n";
                             ca.montoahorro = ca.montoahorro + montoabono - (ca.montoahorro*0.005);
                             cout<<"Monto registrado : "<<ca.montoahorro<<"\n";
                             cout<<"\n Abono efectuado correctamente.";
                             m.IngLista(dnicliente,"Abono",montoabono,ca.montoahorro,ca.cajero.cajero,f.getFecha());
                             ca.verCuentaAhorro();
                       }
                       F2.write((char*)&ca, sizeof(ca));
	                   lec.read((char*)&ca, sizeof(ca)); 
                  }
	              lec.close();
	              F2.close();
	              inFile();
             }
        }
        
        void modificarCliente(char dnicliente[8]){
             CUENTAAHORRO  ca;
             ifstream lec("cuentas.txt",ios::binary);
             ofstream F2("cuentas_tmp.txt", ios::out| ios::trunc|ios::binary);
             if(!lec){  cout<<"\n ERROR al abrir "; }
             else {
                  lec.read((char*)&ca, sizeof(ca));
                  while(!lec.eof()){
                      if(ca.buscarCliente(dnicliente)){
                          for(int i = 0; i<ca.nclientes;i++){
                              if(strncmp(ca.cliente[i].getDNI(),dnicliente,8)==0){
                                   ca.cliente[i].IngresarCliente();
                              }
                          }
                      }
                      F2.write((char*)&ca, sizeof(ca));
                      lec.read((char*)&ca, sizeof(ca));
                 }
             }
             lec.close();
             F2.close();
             inFile();
        }
        void eliminarCuenta(char dnicliente[8]){
             CUENTAAHORRO  ca;
             ifstream lec("cuentas.txt",ios::binary);
             ofstream F2("cuentas_tmp.txt", ios::out| ios::trunc|ios::binary);
             if(!lec){  cout<<"\n ERROR al abrir "; }
             else {
                  lec.read((char*)&ca, sizeof(ca));
                  while(!lec.eof()){
                      if(ca.buscarCliente(dnicliente)){
                          char r;
                          cout<<"�Esta seguro que desea eliminar esta cuenta?Si(s)/No(n)";cin>>r;
                          if(r=='s'){
                              lec.read((char*)&ca, sizeof(ca));
                          }else{
                                F2.write((char*)&ca, sizeof(ca));
                                lec.read((char*)&ca, sizeof(ca));
                          }
                      }else{
                            F2.write((char*)&ca, sizeof(ca));
                            lec.read((char*)&ca, sizeof(ca));
                      }
                 }
             }
             lec.close();
             F2.close();
             inFile();
        }
        void verCuentaAhorro(){
            cout<<"\n ..::Estado de Cuenta::..\n";
            cout<<"\n numero de cuenta: "<<numerocuenta;
            cout<<"\n Cliente(s): \n";
            for(int i=0; i<nclientes;i++){
                cliente[i].MostrarClienteDatos();
            }
            cout<<"\n Monto: "<<montoahorro;
        }
        void printReporteCuentaAhorro(char dnicliente[8]){
           CUENTAAHORRO ca;
           cout<<"\n ..::REPORTE CUENTAS::..\n";
           ifstream lec("cuentas.txt",ios::binary);
             if(!lec){  cout<<"\n ERROR al abrir "; }
             else {
                  lec.read((char*)&ca, sizeof(ca));
                  while(!lec.eof()){
                      if(ca.buscarCliente(dnicliente)){
                            cout<<"\n ..::Estado de Cuenta::..\n";
                            cout<<"\n numero de cuenta: "<<ca.numerocuenta;
                            cout<<"\n Cliente(s): ";
                            for(int i=0;i<ca.nclientes;i++){
                                ca.cliente[i].MostrarClienteDatos();
                            }
                            cout<<"\n Fecha Registro: "<<ca.fecha_registro.getFechaSaved();
                            cout<<"\n Monto: "<<ca.montoahorro;
                      }
                      lec.read((char*)&ca, sizeof(ca));
                  }
             }
             lec.close();
        }
        void printReporteMancomunadas(){
           CUENTAAHORRO ca;
           cout<<"\n ..::Cuentas mancomunadas::..\n";
           ifstream lec("cuentas.txt",ios::binary);
           cout<<"| TIPO | NUMERO | MONTO | FECHA APERTURA | \n";
             if(!lec){  cout<<"\n ERROR al abrir "; }
             else {
                  lec.read((char*)&ca, sizeof(ca));
                  while(!lec.eof()){
                      if(ca.tipoCta=='M'){
                            cout<<ca.tipoCta<<" | ";
                            cout<<ca.numerocuenta<<" | ";
                            cout<<ca.montoahorro<<" | ";
                            cout<<ca.fecha_registro.getFechaSaved()<<" | \n";
                      }
                      lec.read((char*)&ca, sizeof(ca));
                  }
             }
             lec.close();
        }
        void printReporteMovimiento(char dnicliente[8]){
           MOVIMIENTOS m;
           m.VisLista(dnicliente);
        }
        void ingresarCuenta(){
             CUENTAAHORRO ca;
             cout<<"\n";
             cout<<"Seleccione un tipo de cuenta\n";
             do {
              cout<<"\n Ingrese Abono Inicial (Monto Minimo S/.600): ";cin>>ca.montoahorro;}
              while (ca.montoahorro<600);
                     cout<<"\n Tipo Cta (I)ndividual/(M)ancomunada: ";cin>>ca.tipoCta;
                     if (ca.tipoCta=='I'){ ca.nclientes=1; }
                     else { cout<<"\n Cuantas personas integran la Cuenta Mancomunada: "; cin>>ca.nclientes; }
                   ca.cliente = new CLIENTE[ca.nclientes];
                   for (int i=0; i<ca.nclientes; i++){
                       CLIENTE c;
                       c.IngresarCliente();
                       ca.cliente[i] = c;
                   }
              ca.fecha_registro.getFecha();
              cout<<"Monto ingresado : "<<ca.montoahorro<<"\n";
              cout<<"Descuento por apertura : "<<ca.montoahorro*0.005<<"\n";
              ca.montoahorro = ca.montoahorro - (ca.montoahorro*0.005);
              cout<<"Monto registrado : "<<ca.montoahorro<<"\n";
              cout<<"Cuenta creada con exito\n";
               
              ofstream F3("cuentas.txt", ios::out | ios::app | ios::binary);
              if(!F3){  cout<<"\n ERROR al abrir ";    }
              else{
                   F3.write((char*)&ca, sizeof(ca));
              }
	          F3.close();
        }
        void inFile(){
           CUENTAAHORRO ca;
           ifstream F1("cuentas_tmp.txt",ios::binary);
           ofstream F2("cuentas.txt", ios::out| ios::trunc|ios::binary);
           if(!F1){  cout<<"\n ERROR al abrir ";    }
           else{
                 F1.read((char*)&ca, sizeof(ca));
                 while(!F1.eof()){
                    F2.write((char*)&ca, sizeof(ca));
    	            F1.read((char*)&ca, sizeof(ca));
                 }
    	         F1.close();
                 F2.close();    
           }
        }
 };

  
 int main(int argc, char *argv[]){
  CLIENTE cliente_buscado;
  CUENTAAHORRO cuenta;
  CAJERO c;
  char op;
  char bdni[8];
  int cont;
  
  cout<<"\n Usted debe registrar un cajero para poder ingresar al sistema.";
  c.cajeroregistrar();
  do{
      char u[20],p[25];
      cout<<"\n Ingrese usuario:";cin>>u;
      cout<<"\n Ingrese password:";cin>>p;
      if(c.login(u,p) == false){
          cout<<"\n Usuario o contrase�a incorrectas.";
      }
  }while(c.logged == false);
  for(;;){
     cout<<"\n *******************************BANCO ********************************"
     <<"\n <1> Apertura de Cuenta \n <2> Retiro \n <3> Abono \n <4> Eliminar \n<5> Modificar \n <6> Reportes \n <7> Salir \n Ingrese su Opcion: ";
     cin>>op;
     switch(op){
     case '1':cout<<"\n";
              cout<<"\n ******************APERTURA DE CUENTA DE AHORRO********************"<<endl; cont++;
              cuenta.ingresarCuenta();
              cout<<"\n **************************************************************"<<endl;
              break;
     case '2':cout<<"\n";
              cout<<"\n ******************************RETIRO**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              cuenta.retirarDinero(bdni);
              cout<<"\n Gracias.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '3':cout<<"\n";
              cout<<"\n ******************************ABONO**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              cuenta.abonarDinero(bdni);
              cout<<"\n Gracias.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '4':cout<<"\n";
              cout<<"\n ******************************ELIMINAR**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              cuenta.eliminarCuenta(bdni);
              cout<<"\n Cuenta eliminada"<<endl;
              cout<<"\n **************************************************************"<<endl;
              break;
     case '5':cout<<"\n";
              cout<<"\n ******************************Modficar**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              cuenta.modificarCliente(bdni);
              cout<<"\n Gracias.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '6':cout<<"\n *******************************REPORTES********************************"
                        <<"\n <1> Estado de cuenta \n <2> Movimientos efectuados \n <3> Cuentas Mancomunadas \n Ingrese su Opcion: ";
             cin>>op;
             switch(op){
                 case '1':cout<<"\n";
                          cout<<"\n ******************REPORTE DE CUENTA DE AHORRO********************"<<endl;
                          cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
                          cuenta.printReporteCuentaAhorro(bdni);
                          cout<<"\n **************************************************************"<<endl;
                          break;
                 case '2':cout<<"\n";
                          cout<<"\n ******************************MOVIMIENTOS EFECTUADOS**************************"<<endl;
                          cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
                          cuenta.printReporteMovimiento(bdni);
                          cout<<"\n **************************************************************"<<endl;
                          break;
                 case '3':cout<<"\n";
                          cout<<"\n ******************************CUENTAS MANCOMUNIDAS**************************"<<endl;
                          cuenta.printReporteMancomunadas();
                          cout<<"\n **************************************************************"<<endl;
                          break;
             }
              cout<<"\n **************************************************************"<<endl;
              break;
     case '7':cout<<"\n";
              cout<<"\n **************Gracias por usar el sistema.*************"<<endl;
              cout<<"\n ************************Bye**************************"<<endl;
              return 0;
     }
 }
}
