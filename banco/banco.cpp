#include <conio.h>
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

/*Cajero*/
class CAJERO{   
    public:
   	    char dni[8];
        char apellido[255];
        char nombre[255];
        char cajero[25];
        char pass[20];
        bool logged; 
        CAJERO (){logged = false;} 
    	bool login(char iscajero[25], char ispass[20]){
    	  if(strncmp(cajero,iscajero,25)==0 && strncmp(pass,ispass,20)==0){
              logged = true;
              cout<<"\n Bienvenido Sr.: "<<nombre<<", "<<apellido;
              return true;
          }
          return false;
    	}
    	void cajeroregistrar(){
            cout<<"\n DNI: ";cin>>dni;
            cout<<"\n Apellidos: ";cin>>apellido;
            cout<<"\n Nombres: ";cin>>nombre;
            cout<<"\n usuario: ";cin>>cajero;
            cout<<"\n password: ";cin>>pass;
        }
};

class FECHA{
  public:
  int dia,mes,anio;
    FECHA(){}
      void IngDatos(){
    	  cout<<"\n FECHA";
    	  do{
             cout<<"\n Dia: "; cin>>dia;
	         cout<<"\n Mes: "; cin>>mes;
        	 cout<<"\n Anio: "; cin>>anio;
          }while((dia<1 && dia>31) || (mes<1 && mes>12) || (anio<1950 && anio>2011));
      }
      void MosDatos(){
	  cout<<"\n"<<dia<<" "<<mes<<" "<<anio;
      }
};

/*Cliente*/
class CLIENTE{
    char apellido[300];
    char nombre[300];
    char direccion[600];
    public:
    char dni[8];
	void MostrarClienteDatos(){
         cout<<"DNI :"<<dni<<"\n";
         cout<<"Cliente :"<<apellido<<nombre<<"\n";
         cout<<"Direcci�n :"<<direccion<<"\n";
    }
	void MostrarCliente(){
         cout<<"DNI :"<<dni<<"\n";
         cout<<"Cliente :"<<apellido<<nombre<<"\n";
    }
	void IngresarCliente(){
        cout<<"\n DNI: ";cin>>dni;
        cout<<"\n Apellidos: ";cin>>apellido;
        cout<<"\n Nombres: ";cin>>nombre;
        cout<<"\n Domiclio: ";cin>>direccion;
    }
};

/*Clientes de una cuenta*/
class LISTACLIENTE{
    CLIENTE a;
    LISTACLIENTE *sgte,*raiz;
  public:
    char dnibuscar[8];
    LISTACLIENTE(){ raiz=NULL; }
    void IngLista(CLIENTE x){
         LISTACLIENTE *n=new LISTACLIENTE;
         n->a=x;
         if(raiz==NULL){
             raiz=n;
             n->sgte=NULL; }
         else {
              n->sgte=raiz;
              raiz=n;
         } 
    }
    void VisLista(){
       LISTACLIENTE *q=raiz;
       while(q!=NULL){
         q->a.MostrarCliente();
         q=q->sgte;
         }             
    }
    void VistaReporte(){
       LISTACLIENTE *q=raiz;
       while(q!=NULL){
         q->a.MostrarClienteDatos();
         q=q->sgte;
         }             
    }
    bool getClienteCuenta(){
       LISTACLIENTE *q=raiz;
       while(q!=NULL){
        char bdni[8];
        strcpy(bdni,q->a.dni);
        if(strncmp(bdni,dnibuscar,8)==0){
             cout<<"..::Cliente::..\n";
             q->a.MostrarCliente();
             return true;
         }
         q=q->sgte;               
       }
       return false;
    }
    bool modificarClienteCuenta(){
       LISTACLIENTE *q=raiz;
       while(q!=NULL){
        char bdni[8];
        strcpy(bdni,q->a.dni);
        if(strncmp(bdni,dnibuscar,8)==0){
             cout<<"..::Cliente::..\n";
             q->a.IngresarCliente();
             return true;
         }
         q=q->sgte;               
       }
       return false;
    }
 };

/*Movimientos*/
class MOVIMIENTOS{
    public:
           MOVIMIENTOS(){}
        char mdni[8];
        char movimiento[15];
        double monto;
        double total;
        char cajero[30];
        char fecha[20];
        MOVIMIENTOS *sgte,*raiz;
        
        void IngLista( char d[8], char mo[15], double m,double t, char ca[30], char f[20]){
            MOVIMIENTOS *n=new MOVIMIENTOS;
            strcpy(n->mdni,d);
            strcpy(n->movimiento,mo);
            strcpy(n->cajero,ca);
            strcpy(n->fecha,f);
            n->monto = m;
            n->total = t;
            if(raiz==NULL){
                raiz=n;
                n->sgte=NULL;
            } else {
                n->sgte=raiz;
                raiz=n;
            } 
        }
        void VisLista(){
            MOVIMIENTOS *q=raiz;
            while(q!=NULL){
                cout<<"*********REPORTE DE MOVIMIENTOS******************\n";
                cout<<"|   DNI  |        CLIENTE      |     MONTO    |   TOTAL    |   CAJERO   |   FECHA   |";
                cout<<"|   "<<q->mdni<<"  | "<<q->movimiento<<" | "<<q->monto<<" | "<<q->total<<" |  "<<q->cajero<<"  |  "<<q->fecha<<"  |";
                q=q->sgte;
            }             
        }
};  

/*Cuenta de Ahorro*/
class CUENTAAHORRO{  
    public:
        char tipoCta;
        FECHA fecha_registro;
        LISTACLIENTE *cliente;
        CAJERO cajero;
        int numerocuenta;
        double montoahorro;
        CUENTAAHORRO *sgte,*raiz;
        MOVIMIENTOS *m;
        CUENTAAHORRO(){ raiz=NULL; }
        void IngLista( char t, FECHA f, LISTACLIENTE *lc,double m, CAJERO ca, int c){
             CUENTAAHORRO *n=new CUENTAAHORRO;
             n->tipoCta = t;
             n->fecha_registro = f;
             n->cliente = lc;
             n->cajero = ca;
             n->numerocuenta = c;
             n->montoahorro = m;
             if(raiz==NULL){
                 raiz=n;
                 n->sgte=NULL;
             } else {
                  n->sgte=raiz;
                  raiz=n;
             } 
        }
        bool retirarDinero(char dnicliente[8]){
           CUENTAAHORRO *q=raiz;
           m =  new MOVIMIENTOS();
           double montoretiro;
           while(q!=NULL){
            strcpy(q->cliente->dnibuscar,dnicliente);
            if(q->cliente->getClienteCuenta()){
                 do{
                    cout<<"\n Ingrese monto a retirar";
                    cin>>montoretiro;
                 }while(montoretiro > q->montoahorro);
                 cout<<"Monto Actual : "<<q->montoahorro<<"\n";
                 cout<<"Monto a retirar : "<<montoretiro<<"\n";
                 cout<<"Descuento por retiro : "<<q->montoahorro*0.005<<"\n";
                 q->montoahorro = q->montoahorro - (q->montoahorro*0.005) - montoretiro;
                 cout<<"Monto registrado : "<<q->montoahorro<<"\n";
                 cout<<"\n Retiro efectuado correctamente.";
                 strcpy(m->mdni,dnicliente);
                 strcpy(m->movimiento,"Retiro");
                 strcpy(m->cajero,q->cajero.cajero);
//                 strcpy(m->fecha,q->fecha_registro);
                 m->monto = montoretiro;
                 m->total = q->montoahorro;
                 m->IngLista(m->mdni,m->movimiento,montoretiro,m->total,m->cajero,m->fecha);
                 q->verCuentaAhorro();
                 return true;
             }
             q=q->sgte;               
           }
           return false;
        }
        bool abonarDinero(char dnicliente[8]){
           CUENTAAHORRO *q=raiz;
           m =  new MOVIMIENTOS();
           double montoabono;
           while(q!=NULL){
            strcpy(q->cliente->dnibuscar,dnicliente);
            if(q->cliente->getClienteCuenta()){
                 do{
                    cout<<"\n Ingrese monto a abonar";
                    cin>>montoabono;
                 }while(montoabono < 1);
                 cout<<"Monto Actual : "<<q->montoahorro<<"\n";
                 cout<<"Monto a abonar : "<<montoabono<<"\n";
                 cout<<"Descuento por abono : "<<q->montoahorro*0.005<<"\n";
                 q->montoahorro = q->montoahorro + montoabono - (q->montoahorro*0.005);
                 cout<<"Monto registrado : "<<q->montoahorro<<"\n";
                 cout<<"\n Abono efectuado correctamente.";
                 strcpy(m->mdni,dnicliente);
                 strcpy(m->movimiento,"Abono");
                 strcpy(m->cajero,q->cajero.cajero);
//                 strcpy(m->fecha,q->fecha_registro);
                 m->monto = montoabono;
                 m->total = q->montoahorro;
                 m->IngLista(m->mdni,m->movimiento,montoabono,m->total,m->cajero,m->fecha);
                 q->verCuentaAhorro();
                 return true;
             }
             q=q->sgte;               
           }
           return false;
        }
        bool modificarCliente(char dnicliente[8]){
           CUENTAAHORRO *q=raiz;
           while(q!=NULL){
            strcpy(q->cliente->dnibuscar,dnicliente);
            if(q->cliente->modificarClienteCuenta()){
                 return true;
             }
             q=q->sgte;               
           }
           return false;
        }
        bool eliminarCuenta(char dnicliente[8]){
           CUENTAAHORRO *q=raiz;
           bool yesornot;
           while(q!=NULL){
            strcpy(q->cliente->dnibuscar,dnicliente);
            if(q->cliente->getClienteCuenta()){
                 q->verCuentaAhorro();
                 cout<<"\n �Esta seguro que desea eliminar esta cuenta?(1)Si / (0)No";
                 cin>>yesornot;
                 if(yesornot){
                     delete(q);
                     return true;
                 }
             }
             q=q->sgte;               
           }
           return false;
        }
        void verCuentaAhorro(){
            cout<<"\n ..::Estado de Cuenta::..\n";
            cout<<"\n numero de cuenta: "<<numerocuenta;
            cout<<"\n Cliente(s): \n";
            cliente->VisLista();
            cout<<"\n Monto: "<<montoahorro;
        }
        void printReporteCuentaAhorro(char dnicliente[8]){
           CUENTAAHORRO *q=raiz;
           while(q!=NULL){
            strcpy(q->cliente->dnibuscar,dnicliente);
            if(q->cliente->getClienteCuenta()){
                 cout<<"\n ..::Estado de Cuenta::..\n";
                cout<<"\n numero de cuenta: "<<q->numerocuenta;
                cout<<"\n Cliente(s): ";
                q->cliente->VistaReporte();
                cout<<"\n Fecha Registro: ";
                q->fecha_registro.MosDatos();
                cout<<"\n Monto: "<<q->montoahorro;
             }
             q=q->sgte;               
           }
        }
        void printReporteMancomunadas(){
           CUENTAAHORRO *q=raiz;
           cout<<"\n ..::Cuentas mancomunadas::..\n";
           while(q!=NULL){
               cout<<q->tipoCta<<" | ";
               cout<<q->numerocuenta<<" | ";
               cout<<q->montoahorro<<" | \n";
             q=q->sgte;               
           }
        }
        void printReporteMovimiento(char dnicliente[8]){
           CUENTAAHORRO *q=raiz;
           while(q!=NULL){
            strcpy(q->cliente->dnibuscar,dnicliente);
            if(q->cliente->getClienteCuenta()){
                 cout<<"\n ..::Movimientos::..\n";
                q->m->VisLista();
             }
             q=q->sgte;               
           }
        }
        void ingresarCuenta(){
             int n = 0;
             cout<<"\n";
             cout<<"Seleccione un tipo de cuenta\n";
             do {
              cout<<"\n Ingrese Abono Inicial (Monto Minimo S/.600): ";cin>>montoahorro;}
              while (montoahorro<600);
                     cout<<"\n Tipo Cta (I)ndividual/(M)ancomunada: ";cin>>tipoCta;
                     if (tipoCta=='I'){  n=1;  }
                     else { cout<<"\n Cuantas personas integran la Cuenta Mancomunada: "; cin>>n;   }
                   cliente = new LISTACLIENTE();
                   for (int i=0; i<n; i++){
                       CLIENTE c;
                       c.IngresarCliente();
                       cliente->IngLista(c);
                   }
              fecha_registro.IngDatos();
              cout<<"Monto ingresado : "<<montoahorro<<"\n";
              cout<<"Descuento por apertura : "<<montoahorro*0.005<<"\n";
              montoahorro = montoahorro - (montoahorro*0.005);
              cout<<"Monto registrado : "<<montoahorro<<"\n";
              cout<<"Cuenta creada con exito\n";
        }
 };

  
 int main(int argc, char *argv[]){
  CLIENTE cliente_buscado;
  LISTACLIENTE list;
  CUENTAAHORRO cuenta;
  CAJERO c;
  char op;
  char bdni[8];
  int cont;
  
  cout<<"\n Usted debe registrar un cajero para poder ingresar al sistema.";
  c.cajeroregistrar();
  do{
      char u[20],p[25];
      cout<<"\n Ingrese usuario:";cin>>u;
      cout<<"\n Ingrese password:";cin>>p;
      if(c.login(u,p) == false){
          cout<<"\n Usuario o contrase�a incorrectas.";
      }
  }while(c.logged == false);
  for(;;){
     cout<<"\n *******************************BANCO ********************************"
     <<"\n <1> Apertura de Cuenta \n <2> Retiro \n <3> Abono  \n <4> Eliminar  \n<5> Modificar  \n <6> Reportes  \n <7> Salir \n Ingrese su Opcion: ";
     cin>>op;
     switch(op){
     case '1':cout<<"\n";
              cout<<"\n ******************APERTURA DE CUENTA DE AHORRO********************"<<endl; cont++;
              cuenta.ingresarCuenta();
              cuenta.IngLista(cuenta.tipoCta, cuenta.fecha_registro, cuenta.cliente, cuenta.montoahorro, c, cont++);
              cout<<"\n **************************************************************"<<endl;
              break;
     case '2':cout<<"\n";
              cout<<"\n ******************************RETIRO**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              if(cuenta.retirarDinero(bdni)){
                 cout<<"\n Gracias.";
              }else
                cout<<"\n Lo sentimos ocurrio un error.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '3':cout<<"\n";
              cout<<"\n ******************************ABONO**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              if(cuenta.abonarDinero(bdni)){
                 cout<<"\n Gracias.";
              }else
                cout<<"\n Lo sentimos ocurrio un error.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '4':cout<<"\n";
              cout<<"\n ******************************ELIMINAR**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              if(cuenta.eliminarCuenta(bdni)){
                 cout<<"\n Gracias.";
              }else
                cout<<"\n Lo sentimos ocurrio un error.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '5':cout<<"\n";
              cout<<"\n ******************************Modficar**************************"<<endl;
              cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
              if(cuenta.modificarCliente(bdni)){
                 cout<<"\n Gracias.";
              }else
                cout<<"\n Lo sentimos ocurrio un error.";
              cout<<"\n **************************************************************"<<endl;
              break;
     case '6':cout<<"\n *******************************REPORTES********************************"
                        <<"\n <1> Estado de cuenta \n <2> Movimientos efectuados \n <3> Cuentas Mancomunadas  \n Ingrese su Opcion: ";
             cin>>op;
             switch(op){
                 case '1':cout<<"\n";
                          cout<<"\n ******************REPORTE DE CUENTA DE AHORRO********************"<<endl;
                          cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
                          cuenta.printReporteCuentaAhorro(bdni);
                          cout<<"\n **************************************************************"<<endl;
                          break;
                 case '2':cout<<"\n";
                          cout<<"\n ******************************MOVIMIENTOS EFECTUADOS**************************"<<endl;
                          cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
                          cuenta.printReporteMovimiento(bdni);
                          cout<<"\n **************************************************************"<<endl;
                          break;
                 case '3':cout<<"\n";
                          cout<<"\n ******************************CUENTAS MANCOMUNIDAS**************************"<<endl;
                          cout<<"\n Ingrese numero de DNI: "; cin>>bdni;
                          cuenta.printReporteMancomunadas();
                          cout<<"\n **************************************************************"<<endl;
                          break;
             }
              cout<<"\n **************************************************************"<<endl;
              break;
     case '7':cout<<"\n";
              cout<<"\n **************Gracias por usar el sistema.*************"<<endl;
              cout<<"\n ************************Bye**************************"<<endl;
              return 0;
     }
 }
}
